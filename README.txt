# metagraphos

First we create account in gitlab and the desired group and repo names.
The main information needed for the system to work is the group name and project (repository) name created in gitLab and are set in the file public/conf.txt

In our demo the chosen names are:
- group namespace='ellada'
- project name='humanities'

We do not update the main folders of the system as it is shown in .gitignore file so as to keep the repository light.

we can have two options:
a) we place the pdf in folder pdf and run the script 'createEmptySystem.bat'
b) we place the pdf in folder pdf and run the script 'createOCRSystem.bat' if we want tesseract to be applied

afterwards we run the 'gitActions' script the system is ready for use after a couple of minutes.

In the web interface 'ellada/humanities'  is passed as information in the following demo URL.

Examples
a) Local example using the gitLab api after running the web server caddy.exe (serves at https://localhost:2015)

https://localhost:2015/public/index.html?ellada/humanities&page=img-01.png_307044145387956.txt

b) Static gitLabPages using the gitLab api
https://ellada.gitlab.io/humanities/index.html?ellada/humanities&page=img-01.png_307044145387956.txt


for admin purposes there is an URL 
https://localhost:2015/public/admin.html?ellada/humanities
that gives information about all the links for transcription and the bytes of the files
